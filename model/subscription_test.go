package model_test

import "github.com/yezooz/common/model"

func GetTestSubscription() *model.Subscription {
	return &model.Subscription{
		Id:     10,
		Source: GetTestSource(),
	}
}
