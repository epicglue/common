package model

import (
	"github.com/Sirupsen/logrus"
	"github.com/dogenzaka/go-iap/appstore"
	"github.com/yezooz/api/connection/database/postgres"
	"github.com/yezooz/common/helpers"
	"runtime/debug"
	"strconv"
	"time"
)

type UserPayment struct {
	Id                int64
	UserId            int64
	Quantity          int64
	Amount            int64
	NetAmount         int64
	ProductId         string
	PaymentId         string
	OriginalPaymentId string
	PaymentTime       time.Time
	CreatedAt         time.Time
}

func AddPayment(user *User, receipt appstore.InApp) error {
	qty, _ := strconv.ParseInt(receipt.Quantity, 10, 64)
	plan := LoadPlan(receipt.ProductID)

	up := &UserPayment{
		UserId:            user.Id,
		Quantity:          qty,
		Amount:            plan.Amount,
		NetAmount:         plan.NetAmount,
		ProductId:         receipt.ProductID,
		PaymentId:         receipt.TransactionID,
		OriginalPaymentId: receipt.OriginalTransactionID,
		PaymentTime:       helpers.TimeFromTimestampString(receipt.PurchaseDate.PurchaseDateMS),
		CreatedAt:         time.Now(),
	}

	return up.Save()
}

func (up *UserPayment) Save() error {
	start := time.Now()

	query := `
        INSERT INTO
            user_payment (user_id, quantity, amount, net_amount, product_id, payment_id, original_payment_id, payment_time, created_at)
        VALUES
            ($1, $2, $3, $4, $5, $6, $7, $8, NOW())
    `

	conn := postgres.NewPostgres()
	if _, err := conn.DB().Exec(query, up.UserId, up.Quantity, up.Amount, up.NetAmount, up.ProductId, up.PaymentId, up.OriginalPaymentId, up.PaymentTime); err != nil {
		log.WithFields(logrus.Fields{
			"stack":               string(debug.Stack()),
			"query":               query,
			"user_id":             up.UserId,
			"qty":                 up.Quantity,
			"amount":              up.Amount,
			"net_amount":          up.NetAmount,
			"product_id":          up.ProductId,
			"payment_id":          up.PaymentId,
			"original_payment_id": up.OriginalPaymentId,
			"payment_time":        up.PaymentTime.String(),
			"took":                time.Since(start),
			"took_str":            time.Since(start).String(),
		}).Error(err)

		return err
	}

	return nil
}
