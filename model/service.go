package model

import (
	"database/sql"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/api/connection/database/postgres"
	"github.com/yezooz/null"
	"runtime/debug"
)

type Service struct {
	Id          int64     `json:"id,omitempty" gorm:"-"`
	Category    *Category `json:"category,omitempty" gorm:"-"`
	CategoryId  int64     `json:"-" gorm:"-"`
	ShortName   string    `json:"short_name" gorm:"column:service"`
	Name        string    `json:"name,omitempty" gorm:"-"`
	Description string    `json:"description,omitempty" gorm:"-"`
	IsVisible   bool      `json:"-" gorm:"-"`
	CreatedAt   null.Time `json:"-" gorm:"-"`
	UpdatedAt   null.Time `json:"-" gorm:"-"`
}

func LoadService(id int64) *Service {
	query := `
		SELECT
			id,
			category_id,
			short_name,
			name,
			description,
			is_visible,
			created_at,
			updated_at
		FROM
			service
		WHERE
			id = $1
	`

	s := Service{}
	if err := s.mapRow(postgres.NewPostgres().DB().QueryRow(query, id)); err != nil {
		return nil
	} else {
		return &s
	}
}

func LoadServiceByShortName(shortName string) *Service {
	query := `
		SELECT
			id,
			category_id,
			short_name,
			name,
			description,
			is_visible,
			created_at,
			updated_at
		FROM
			service
		WHERE
			short_name = $1
	`

	s := Service{}
	if err := s.mapRow(postgres.NewPostgres().DB().QueryRow(query, shortName)); err != nil {
		return nil
	} else {
		return &s
	}
}

func (s *Service) mapRow(row *sql.Row) error {
	err := row.Scan(
		&s.Id,
		&s.CategoryId,
		&s.ShortName,
		&s.Name,
		&s.Description,
		&s.IsVisible,
		&s.CreatedAt,
		&s.UpdatedAt,
	)

	if err == sql.ErrNoRows {
		return err
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return err
	}

	s.Category = s.GetCategory()

	return nil
}

func ServiceByShortName(name string) *Service {
	query := `
		SELECT
			s.id,
			s.short_name,
			s.description,
			s.is_visible,
			c.name as category_name,
			c.description as category_description,
			cc.name as upper_category_name,
			cc.description as upper_category_description
		FROM
			service AS s
		JOIN
			category AS c on (c.id = s.category_id)
		LEFT JOIN
			category AS cc ON (cc.id = c.upper_category_id)
		WHERE
			s.short_name = $1`

	row := postgres.NewPostgres().DB().QueryRow(query, name)

	if row == nil {
		return nil
	}

	return parseService(row)
}

func parseService(row *sql.Row) *Service {
	var (
		serviceId                int64
		shortName                string
		description              string
		isVisible                bool
		categoryName             string
		categoryDescription      string
		upperCategoryName        null.String
		upperCategoryDescription null.String
	)

	err := row.Scan(
		&serviceId,
		&shortName,
		&description,
		&isVisible,
		&categoryName,
		&categoryDescription,
		&upperCategoryName,
		&upperCategoryDescription,
	)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return nil
	}

	category := &Category{
		Name:        categoryName,
		Description: categoryDescription,
	}

	if upperCategoryName.Valid {
		category.UpperCategory = &Category{
			Name:        upperCategoryName.String,
			Description: upperCategoryDescription.String,
		}
	}

	return &Service{
		Id:          serviceId,
		ShortName:   shortName,
		Description: description,
		IsVisible:   isVisible,
		Category:    category,
	}
}

func (s *Service) GetCategory() *Category {
	if s.Category == nil {
		s.Category = LoadCategory(s.CategoryId)
	}

	return s.Category
}
