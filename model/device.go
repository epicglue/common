package model

type Device struct {
	Name string `json:"name"`
	UDID string `json:"udid"`
}
