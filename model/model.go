package model

type Model interface {
	Save() error
	Delete() error

	//	LoadById(int64) error
}
