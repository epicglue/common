package model

const (
	Ready      = "ready"
	Enqueued   = "enqueued"
	InProgress = "in_progress"
	Failed     = "failed"
)

type Status struct {
	Value string `json:"value"`
}

func StatusFromString(value string) *Status {
	switch value {
	case Ready, Enqueued, InProgress, Failed:
		return &Status{Value: value}
	default:
		return nil
	}
}

func (s *Status) isReady() bool {
	return s.Value == Ready
}

func (s *Status) isEnqueued() bool {
	return s.Value == Enqueued
}

func (s *Status) isInProgress() bool {
	return s.Value == InProgress
}

func (s *Status) isFailed() bool {
	return s.Value == Failed
}

func (s *Status) setAsReady() {
	s.Value = Ready
}

func (s *Status) setAsEnqueued() {
	s.Value = Enqueued
}

func (s *Status) setAsInProgress() {
	s.Value = InProgress
}

func (s *Status) setAsFailed() {
	s.Value = Failed
}
