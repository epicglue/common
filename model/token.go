package model

import "github.com/yezooz/null"

type Token struct {
	Service      *Service
	Identifier   string
	FriendlyName string
	Token        string
	TokenSecret  null.String
	RefreshToken null.String
	Expiry       null.Time
	Scope        null.String
}
