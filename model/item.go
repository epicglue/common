package model

import (
	"crypto/sha256"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/yezooz/common/helpers"
	"github.com/yezooz/common/helpers/pg_helpers"
	"github.com/yezooz/null"
	"time"
)

var log = helpers.GetLogger("model")

type Item struct {
	Id          string `json:"id" gorm:"primary_key"`
	ItemContent `json:"content"`
	ItemType    string      `json:"item_type"`
	MediaType   string      `json:"media_type"`
	Service     string      `json:"service"`
	Title       null.String `json:"title,omitempty"`
	Description null.String `json:"description,omitempty"`
	Author      string      `json:"author"`
	AuthorMedia *Media      `json:"author_media"`
	Media       []*Media    `json:"media,omitempty"`
	Location    *Location   `json:"location,omitempty"`
	Links       `json:"links" gorm:"links"`
	Tags        pg_helpers.StringArray `json:"tags,omitempty"`
	Visibility  string                 `json:"-"`
	Points      null.Int               `json:"point,omitempty"`
	Comments    null.Int               `json:"comments,omitempty"`
	IsRead      bool                   `json:"is_read" gorm:"-"`
	IsGlued     bool                   `json:"is_glued" gorm:"-"`
	SubIdList   []int64                `json:"subs,omitempty" gorm:"-"`
	CreatedAt   *time.Time             `json:"created_at"`
	UpdatedAt   *time.Time             `json:"updated_at,omitempty"`
	DeletedAt   *time.Time             `json:"deleted_at,omitempty" sql:"index"`
	IndexedAt   time.Time              `json:"indexed_at"`
}

// GORM
func (i Item) TableName() string {
	return "item"
}

func (i *Item) BeforeCreate(scope *gorm.Scope) error {
	scope.SetColumn("Id", i.buildHash())

	return nil
}

// Methods
func (i *Item) ToElasticItem() *ElasticItem {
	e := &ElasticItem{
		Id:             i.Id,
		ItemType:       i.ItemType,
		MediaType:      i.MediaType,
		Service:        i.Service,
		Title:          i.Title.String,
		Description:    i.Description.String,
		Author:         i.Author,
		AuthorMedia:    i.AuthorMedia,
		Media:          i.Media,
		Tags:           i.Tags,
		Links:          i.Links,
		Points:         i.Points.Int64,
		Comments:       i.Comments.Int64,
		IsRead:         i.IsRead,
		IsGlued:        i.IsGlued,
		SubIdList:      i.SubIdList,
		CreatedAt:      i.ItemContent.CreatedAt,
		IndexedAt:      time.Now(),
		ContentOrderBy: i.ItemContent.OrderBy,
	}

	if i.ItemContent.UpdatedAt.Valid {
		e.UpdatedAt = &i.ItemContent.UpdatedAt.Time
	}

	return e
}

// TODO: abstract
func (i *Item) GetService() *Service {
	return ServiceByShortName(i.Service)
}

func (i *Item) buildHash() string {
	content_id := i.ItemContent.Id
	if i.ItemContent.SecondaryId.Valid {
		content_id = i.ItemContent.SecondaryId.String
	}

	hasher := sha256.New()
	hasher.Write([]byte(i.Service))
	hasher.Write([]byte(content_id))

	return fmt.Sprintf("%x", hasher.Sum(nil))
}
