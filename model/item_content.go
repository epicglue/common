package model

import (
	"github.com/yezooz/null"
	"time"
)

type ItemContent struct {
	Id          string      `json:"id" gorm:"column:content_id"`
	OrderBy     *time.Time  `json:"order_by" gorm:"column:content_order_by_date"`
	SecondaryId null.String `json:"secondary_id,omitempty" gorm:"column:content_secondary_id"`
	CreatedAt   *time.Time  `json:"created_at,omitempty" gorm:"column:content_created_at"`
	UpdatedAt   null.Time   `json:"updated_at,omitempty" gorm:"column:content_updated_at"`
}
