package model

import "github.com/yezooz/null"

type UserItem struct {
	Id        int64     `json:"id"`
	IsRead    bool      `json:"read"`
	IsGlued   bool      `json:"glued"`
	Item      *Item     `json:"item"`
	User      *User     `json:"user"`
	CreatedAt null.Time `json:"-"`
	UpdatedAt null.Time `json:"-"`
}
