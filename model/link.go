package model

type Links struct {
	Default  string `json:"default" gorm:"column:url"`
	Internal string `json:"internal,default,omitempty" gorm:"column:url_in"`
	External string `json:"external,default,omitempty" gorm:"column:url_ext"`
}
