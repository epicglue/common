package model_test

import "github.com/yezooz/common/model"

func GetTestService() *model.Service {
	return &model.Service{
		Id:        1,
		Category:  GetTestCategory(),
		ShortName: "test_service",
		Name:      "Test Service",
	}
}
