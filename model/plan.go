package model

import (
	"database/sql"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/api/connection/database/postgres"
	"runtime/debug"
	"time"
)

const (
	PLAN_FREE = "free"
	PLAN_BETA = "beta"
	//PLAN_CONTRIBUTOR = "contributor"
	//PLAN_PRO         = "pro"
	//PLAN_UNLIMITED   = "unlimited"
)

type Plan struct {
	Id             int64  `json:"id"`
	ShortName      string `json:"short_name"`
	Name           string `json:"name"`
	Description    string `json:"description"`
	ProductId      string `json:"product_id"`
	TTL            int64  `json:"ttl"`
	TTLDescription string `json:"ttl_description"`
	Amount         int64  `json:"amount"`
	NetAmount      int64  `json:"net_amount"`
	IsVisible      bool   `json:"is_visible"`
}

func LoadPlans() []*Plan {
	start := time.Now()

	query := `
		SELECT
			id,
			short_name,
			name,
			description,
			product_id,
			ttl,
			ttl_description,
			amount,
			net_amount,
			is_visible
		FROM
			plan
		ORDER BY
			amount
	`

	rows, err := postgres.NewPostgres().DB().Query(query)

	if err == sql.ErrNoRows {
		return make([]*Plan, 0)
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"took":     time.Since(start),
			"took_str": time.Since(start).String(),
		}).Error(err)

		return make([]*Plan, 0)
	}

	var (
		plans []*Plan = make([]*Plan, 0)
	)

	for rows.Next() {
		plan := mapRow(rows)

		if plan != nil {
			plans = append(plans, plan)
		}
	}
	rows.Close()

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
	}).Debug("LoadPlans")

	// TODO: caching

	return plans
}

func LoadPlan(productId string) *Plan {
	// TODO: refactor
	for _, plan := range LoadPlans() {
		if plan.ProductId == productId {
			return plan
		}
	}

	log.WithFields(logrus.Fields{
		"stack": string(debug.Stack()),
	}).Errorf("Failed to match %s with product_id", productId)

	return nil
}

func LoadPlanById(planId int64) *Plan {
	// TODO: refactor
	for _, plan := range LoadPlans() {
		if plan.Id == planId {
			return plan
		}
	}

	log.WithFields(logrus.Fields{
		"stack": string(debug.Stack()),
	}).Errorf("Failed to match %d with id", planId)

	return nil
}

func ActivePlanByUser(user *User) *Plan {
	start := time.Now()

	query := `
		SELECT
			plan_id
		FROM
			user_plan
		WHERE
		    user_id = $1 AND
		    is_active = True
		LIMIT 1
	`

	var planId int64
	err := postgres.NewPostgres().DB().QueryRow(query, user.Id).Scan(&planId)

	if err == sql.ErrNoRows {
		return LoadPlan(PLAN_FREE)
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"took":     time.Since(start),
			"took_str": time.Since(start).String(),
		}).Error(err)

		return nil
	}

	return LoadPlanById(planId)
}

func mapRow(rows *sql.Rows) *Plan {
	var (
		id             int64
		shortName      string
		name           string
		description    string
		productId      string
		ttl            int64
		ttlDescription string
		amount         int64
		netAmount      int64
		isVisible      bool
	)

	err := rows.Scan(
		&id,
		&shortName,
		&name,
		&description,
		&productId,
		&ttl,
		&ttlDescription,
		&amount,
		&netAmount,
		&isVisible,
	)

	if err == sql.ErrNoRows {
		return nil
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return nil
	}

	return &Plan{
		Id:             id,
		Name:           name,
		ShortName:      shortName,
		Description:    description,
		ProductId:      productId,
		TTL:            ttl,
		TTLDescription: ttlDescription,
		Amount:         amount,
		NetAmount:      netAmount,
		IsVisible:      isVisible,
	}
}
