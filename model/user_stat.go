package model

import (
	"database/sql"
	"github.com/yezooz/null"
	"time"
)

type UserStat struct {
	User      *User           `json:"user"`
	Profile   *ServiceProfile `json:"profile"`
	Date      time.Time       `json:"date"`
	Key       sql.NullString  `json:"key"`
	Value     sql.NullString  `json:"value"`
	CreatedAt null.Time       `json:"-"`
	UpdatedAt null.Time       `json:"-"`
}
