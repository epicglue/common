package model

import (
	"database/sql"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/api/connection/database/postgres"
	"github.com/yezooz/null"
	"runtime/debug"
)

type ServiceProfile struct {
	Id           int64     `json:"id"`
	User         *User     `json:"user"`
	Service      *Service  `json:"service"`
	Identifier   string    `json:"identifier"`
	FriendlyName string    `json:"friendly_name"`
	CreatedAt    null.Time `json:"-"`
	UpdatedAt    null.Time `json:"-"`
}

func LoadServiceProfile(id int64) *ServiceProfile {
	if id == 0 {
		return nil
	}

	query := `
		SELECT
			id,
			user_id,
			service_id,
			identifier,
			friendly_name,
			created_at,
			updated_at
		FROM
			user_service_profile
		WHERE
			id = $1
	`

	s := ServiceProfile{}
	if err := s.mapRow(postgres.NewPostgres().DB().QueryRow(query, id)); err != nil {
		return nil
	} else {
		return &s
	}
}

func LoadServiceProfileByIdentifier(service *Service, user *User, identifier string) *ServiceProfile {
	query := `
		SELECT
			id,
			user_id,
			service_id,
			identifier,
			friendly_name,
			created_at,
			updated_at
		FROM
			user_service_profile
		WHERE
			user_id = $1 AND
			service_id = $2 AND
			identifier = $3
	`

	s := ServiceProfile{}
	if err := s.mapRow(postgres.NewPostgres().DB().QueryRow(query, user.Id, service.Id, identifier)); err != nil {
		return nil
	} else {
		return &s
	}
}

func (s *ServiceProfile) mapRow(row *sql.Row) error {
	var user_id int64
	var service_id int64

	err := row.Scan(
		&s.Id,
		&user_id,
		&service_id,
		&s.Identifier,
		&s.FriendlyName,
		&s.CreatedAt,
		&s.UpdatedAt,
	)

	if err == sql.ErrNoRows {
		return err
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return err
	}

	s.User = LoadUser(user_id)
	s.Service = LoadService(service_id)

	if s.User == nil || s.Service == nil {
		panic("Failed to ServiceProfile.mapRow")
	}

	return nil
}
