package model_test

import "github.com/yezooz/common/model"

func GetTestSource() *model.Source {
	return &model.Source{
		Id:      5,
		Service: GetTestService(),
	}
}
