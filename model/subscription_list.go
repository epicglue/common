package model

type SubscriptionListResponse struct {
	Services []*SubscriptionListService `json:"services"`
}

func (s *SubscriptionListResponse) HasService(service *SubscriptionListService) bool {
	for _, sx := range s.Services {
		if sx.Id == service.Id {
			return true
		}
	}

	return false
}

type SubscriptionListService struct {
	Id          int64                     `json:"id"`
	Name        string                    `json:"name"`
	Description string                    `json:"description"`
	Category    *SubscriptionListCategory `json:"category"`
	Sources     []*SubscriptionListSource `json:"sources"`
}

func (s *SubscriptionListService) HasSource(source *SubscriptionListSource) bool {
	for _, sx := range s.Sources {
		if sx.Id == source.Id {
			return true
		}
	}

	return false
}

type SubscriptionListCategory struct {
	Id   int64
	Name string
}

type SubscriptionListSource struct {
	Id            int64                           `json:"id"`
	Name          string                          `json:"name"`
	Description   string                          `json:"description"`
	Filter        string                          `json:"filter"`
	Value         string                          `json:"value"`
	Count         int64                           `json:"count"`
	Subscriptions []*SubscriptionListSubscription `json:"subscriptions"`
}

func (s *SubscriptionListSource) HasSubscription(sub *SubscriptionListSubscription) bool {
	for _, sx := range s.Subscriptions {
		if sx.Id == sub.Id {
			return true
		}
	}

	return false
}

type SubscriptionListSubscription struct {
	Id            int64  `json:"id"`
	Name          string `json:"name"`
	Description   string `json:"description"`
	Value         string `json:"value"`
	Count         int64  `json:"count"`
	HasSubscribed bool   `json:"has_subscribed"`
}
