package model

import "github.com/yezooz/null"

type State struct {
	Subscription *Subscription `json:"subscription"`
	Value        string        `json:"value"`
	CreatedAt    null.Time     `json:"-"`
	UpdatedAt    null.Time     `json:"-"`
}
