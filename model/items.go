package model

import (
	"encoding/json"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/mattbaird/elastigo/lib"
	"runtime/debug"
	"strings"
	"time"
)

type Items struct {
	Items []*Item `json:"items"`
}

func ItemsFromHits(hits *elastigo.Hits) (*Items, error) {
	startedAt := time.Now()
	items := Items{}

	for _, hit := range hits.Hits {
		tempItem := ElasticItem{}
		if err := json.Unmarshal(*hit.Source, &tempItem); err != nil {
			log.WithFields(logrus.Fields{
				"stack":    string(debug.Stack()),
				"took":     time.Since(startedAt),
				"took_str": time.Since(startedAt).String(),
			}).Error(err)

			continue
		}

		items.AddItem(tempItem.ToItem())
	}

	return &items, nil
}

func (i *Items) AddItem(item *Item) {
	i.Items = append(i.Items, item)
}

func (i *Items) JoinHashes() string {
	items := []string{}
	for _, item := range i.Items {
		items = append(items, item.Id)
	}

	return fmt.Sprintf(`'%s'`, strings.Join(items, "','"))
}
