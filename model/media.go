package model

import "github.com/yezooz/common/helpers/pg_helpers"

type Media struct {
	pg_helpers.JSONB

	Original  *Medium `json:"original,omitempty"`
	Large     *Medium `json:"large,omitempty"`
	Medium    *Medium `json:"medium,omitempty"`
	Small     *Medium `json:"small,omitempty"`
	Thumbnail *Medium `json:"thumbnail,omitempty"`
}

type Medium struct {
	Url      string  `json:"url"`
	CacheUrl string  `json:"cache_url,omitempty"`
	Width    int64   `json:"width,omitempty"`
	Height   int64   `json:"height,omitempty"`
	Preview  *Medium `json:"preview,omitempty"`
}
