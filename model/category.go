package model

import (
	"database/sql"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/api/connection/database/postgres"
	"github.com/yezooz/null"
	"runtime/debug"
	"time"
)

type Category struct {
	Id            int64     `json:"id"`
	UpperCategory *Category `json:"upper_category,omitempty"`
	Name          string    `json:"name,omitempty"`
	Description   string    `json:"description,omitempty"`
	IsVisible     bool      `json:"-"`
	CreatedAt     null.Time `json:"-"`
	UpdatedAt     null.Time `json:"-"`
}

func LoadCategory(id int64) *Category {
	query := `
		SELECT
			id,
			upper_category_id,
			name,
			description,
			is_visible,
			created_at,
			updated_at
		FROM
			category
		WHERE
			id = $1
	`

	c := Category{}
	if err := c.mapRow(postgres.NewPostgres().DB().QueryRow(query, id)); err != nil {
		return nil
	} else {
		return &c
	}
}

func (c *Category) mapRow(row *sql.Row) error {
	start := time.Now()

	var upper_category_id null.Int

	err := row.Scan(
		&c.Id,
		&upper_category_id,
		&c.Name,
		&c.Description,
		&c.IsVisible,
		&c.CreatedAt,
		&c.UpdatedAt,
	)

	if err == sql.ErrNoRows {
		return err
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"took":     time.Since(start),
			"took_str": time.Since(start).String(),
			"stack":    string(debug.Stack()),
		}).Error(err)

		return err
	}

	if upper_category_id.Valid {
		c.UpperCategory = LoadCategory(upper_category_id.Int64)
	}

	return nil
}

func (c *Category) Save() error {
	log.Fatal("Not Implemented")
	return nil
}

func (c *Category) Delete() error {
	log.Fatal("Not Implemented")
	return nil
}
