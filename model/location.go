package model

import "github.com/yezooz/common/helpers/pg_helpers"

type Location struct {
	pg_helpers.JSONB

	Name string  `json:"name,omitempty"`
	Lat  float64 `json:"lat,omitempty"`
	Lon  float64 `json:"lon,omitempty"`
}
