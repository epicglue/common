package test_helper

import (
	"github.com/yezooz/common/helpers"
	"github.com/yezooz/common/model"
	"github.com/yezooz/null"
)

func MakeTestUser() model.User {
	freshUser := model.User{
		Email:    helpers.RandomEmail(),
		Password: null.StringFrom(helpers.RandomString(64)),
	}

	return freshUser
}
