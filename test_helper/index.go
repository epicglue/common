package test_helper

import "github.com/yezooz/api/connection"

func HasIndex(indexName string) bool {
	es := connection.NewConnector().GetES()

	has, err := es.IndicesExists(indexName)

	return has == true && err == nil
}
