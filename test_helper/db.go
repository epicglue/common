package test_helper

import (
	"fmt"
	"github.com/yezooz/api/connection/database/postgres"
)

func HasRecordsInTable(tableName string) bool {
	var count int

	postgres.NewPostgres().DB().QueryRow(fmt.Sprintf(`
		SELECT
			COUNT(*) AS c
		FROM
			"%s"
	`, tableName)).Scan(&count)

	return count > 0
}

func HasRecordsInTableWithCondition(tableName string, condition string) bool {
	var count int

	query := fmt.Sprintf(`
		SELECT
			COUNT(*) AS c
		FROM
			"%s"
		WHERE
			%s
	`, tableName, condition)

	postgres.NewPostgres().DB().QueryRow(query).Scan(&count)

	return count > 0
}
