package pg_helpers

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"strconv"
	"strings"
)

type StringArray []string

func (i StringArray) Value() (driver.Value, error) {
	bits := []string{}

	for _, v := range i {
		if d, err := json.Marshal(v); err != nil {
			return nil, err
		} else {
			bits = append(bits, strconv.Quote(string(d)))
		}
	}

	v := []byte("{" + strings.Join(bits, ",") + "}")

	return v, nil
}

func (i *StringArray) Scan(input interface{}) error {
	b, ok := input.([]byte)
	if !ok {
		return errors.New("Invalid input")
	}

	if string(b) == "{}" {
		var bits []string
		*i = bits

		return nil
	}

	c := strings.Split(string(b[1:len(b)-1]), ",")
	b = []byte(`["` + strings.Join(c, `","`) + `"]`)

	var bits []string
	if err := json.Unmarshal(b, &bits); err != nil {
		return err
	}

	*i = bits

	//for _, s := range bits {
	//    var v string
	//    if err := json.Unmarshal([]byte(s), &v); err != nil {
	//        return err
	//    }
	//
	//    *i = append(*i, v)
	//}

	return nil
}
